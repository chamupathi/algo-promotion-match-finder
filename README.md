# algo-promotion-match-finder

#### this algo is to solve following question,

- There are some items in a cart,
- Marketting team wants to give a gift to a person if there is certain items groups are in order in a particular cart.
- Marketing team will update item groups each day
- if all the item groups are in a cart ( in order / non repeat / non overlapping ) that cart will be choosen as eligible for a price.
- if an item marked as `anything` any item can match that posision
- write an algorigthm to finf whether a given cart is eligible or not.


---
### Example 1

cart : 
"orange", "apple", "apple", "banana", "orange", "apple", "banana"

match Gruops : 
"orange apple", "banana"

#### solution
There are two match groups
1. "orange apple"
2. "banana"


cart matches with "orange apple"

cart matches with "banana" ( even after the group 1 matches in order)

the cart fulfills both match groups 1 and 2, without items overlapping, hence this is eligible for the promotion

---
### Example 2

cart : 
"orange", "apple", "apple", "banana", "orange", "apple", "banana"

match Gruops : 
"orange apple", "banana orange"

#### solution
There are two match groups
1. "orange apple"
2. "banana orange"


cart matches with "orange apple"

cart matches with "banana orange" ( even after the group 1 matches in order)

the cart fulfills both match groups 1 and 2, without items overlapping, hence this is eligible for the promotion

### Example 3

cart : 
"orange", "apple", "apple", "banana", "orange", "apple", "banana"

match Gruops : 
"apple apple", "anything orange"

#### solution
There are two match groups
1. "apple apple"
2. "anything orange"


cart matches with "apple apple"

cart matches with "anything orange" ( even after the group 1 matches in order)

the cart fulfills both match groups 1 and 2, without items overlapping, hence this is eligible for the promotion

### Example 4

cart : 
"orange", "apple", "banana"

match Gruops : 
"orange apple", "apple banana"

#### solution
There are two match groups
1. "orange apple"
2. "apple banana"


cart matches with "orange apple"

cart matches with "apple banana" ( but fails since "apple" in posision 2 is currenly added to another group)

hence this is not eligible for the promotion