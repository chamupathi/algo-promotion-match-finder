import java.util.List;

public class Logic {
        public static String convertStringToHash(String s) {
                if (Constants.ANYTHING.equals(s)) {
                    return Util.getStringWithLengthAndFilledWithCharacter("", Constants.ACCURACY, '.');
                }
        
                if(s.length() >= Constants.ACCURACY) {
                    return s.substring(0, Constants.ACCURACY);
                }else {
                     return Util.getStringWithLengthAndFilledWithCharacter(s, Constants.ACCURACY, Constants.FILL_CHAR);
                }
            }
        
            public static String getRegexString(List<String> patter) {
                String str = "";
                for ( String s : patter) {
                    str += convertStringToHash(s);
                }
        
                return str;
            }
}
