import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import java.util.regex.Matcher;


class Main {

    public static boolean isEligibleForPromo(List<String> codeGroupList, List<String> shoppingCart) {

        String cartString = "";
        for ( String s : shoppingCart) {
            cartString += Logic.convertStringToHash(s);
        }

        cartString = Logic.getRegexString(shoppingCart);

        for ( String group : codeGroupList) {

            String[] codes = group.split(" ");

            String codesRegex = Logic.getRegexString(Util.stringArrToList(codes));

            Pattern pattern = Pattern.compile(codesRegex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(cartString);
            boolean matchFound = matcher.find();

            if( !matchFound )
                return false;

            // remove match string
            cartString = matcher.replaceFirst(Util.getStringWithLengthAndFilledWithCharacter("",codesRegex.length(), Constants.REPLACER ));
            cartString = cartString.replaceAll(String.valueOf(Constants.REPLACER), "");
        }

        return true;
    }


    public static void main(String[] args) {

        List<String> groupsList =  new ArrayList<String>();  
        List<String> cartList =  new ArrayList<String>();  

        // String[] groups = {"orange", "anything apple", "banana orange apple", "banana"}; // => "o", "aa", "boa", "..."
        // String[] cart = {"orange", "apple", "apple", "banana", "orange", "apple", "banana"}; // => "oraaaboab"

        // String[] groups = {"anything", "anything banana"}; // => "o", "aa", "boa", "..."
        // String[] cart = {"orange", "banana", "banana"}; // => "oraaaboab"

        // String[] groups = {"anything", "anything banana", "anything"}; // => "o", "aa", "boa", "..."
        // String[] cart = {"orange", "banana", "banana"}; // => "oraaaboab"

        // String[] groups = {"anything", "anything banana", "anything"}; // => "o", "aa", "boa", "..."
        // String[] cart = {"orange", "banana", "banana", "papol", "papadam", "saman"}; // => "oraaaboab"

        String[] groups = {"anything", "anything banana", "saman anything"}; // => "o", "aa", "boa", "..."
        String[] cart = {"orange", "banana", "banana", "papol", "papadam", "saman", "parippu"}; // => "oraaaboab"
        

        groupsList = Util.stringArrToList(groups);
        cartList = Util.stringArrToList(cart);

        boolean result = isEligibleForPromo(groupsList, cartList);
        System.out.println(result);
    }
}