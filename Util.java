import java.util.ArrayList;
import java.util.List;

public class Util {
        static String getStringWithLengthAndFilledWithCharacter(String s, int length, char charToFill) {
                char[] array = new char[length];
        
                for (int i=0; i<s.length();i++) {
                    array[i] = s.charAt(i);
                }
        
                int pos = s.length();
                while (pos < length) {
                    array[pos] = charToFill;
                    pos++;
                }
        
                return new String(array);
            }

        static List<String> stringArrToList(String[] arr) {
                List<String> list =  new ArrayList<String>();
        
                for (String a : arr) {
                    list.add(a);
                }
        
                return list;
        }
}
