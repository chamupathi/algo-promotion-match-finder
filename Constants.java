public class Constants {
        static final int ACCURACY = 8;
        static final char FILL_CHAR = '@';
        static final String ANYTHING = "anything";
        static final char REPLACER = '!';
}
